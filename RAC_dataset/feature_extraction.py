import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

label = []
sentences = []
time = []

with open("./all.dat", "r") as in_fp:
    
    for line in in_fp:

        line = line.strip().split("\t")
            
        print line

        if line[2] == "SPEC": label.append(1)
        else: label.append(0)
        
        time.append(int(line[1]))

        sentences.append(line[3])

vectorizer = CountVectorizer(min_df=2, stop_words="english")
X = vectorizer.fit_transform(sentences)
X = X.todense()
print X.shape

data = np.concatenate((np.asarray([label]).T, X), axis=1)
print data.shape

np.savetxt("clean_all.dat", data, fmt="%d", delimiter=",")
np.savetxt("clean_all.time", time, fmt="%d")
