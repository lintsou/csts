import numpy as np

data = open("./all.dat", "r")
tags = open("./clean_all.dat", "r")
out = open("./hurest.time", "w")

for line, features in zip(data, tags):
    leng = len(line.strip().split(" ")) - 3
    tag = sum(map(int, features.strip().split(",")))
    time = 3.8*leng + 5.39*tag + 12.57
    out.write(str(time)+"\n")
