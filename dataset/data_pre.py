import numpy as np
from collections import Counter

'''
data_name = ["Australian", "breast", "diabetes", "german", "glass", "heart", "liver", "sonar", "vehicle", "vote"]
        
for name in data_name:
    
    data = np.loadtxt(name+"_all.csv", delimiter=",", dtype=str)
    np.savetxt(name+".csv", data, delimiter=",", fmt="%s")
'''

#end_data = ["abalone", "yeast", "ttt", "nursey", "seeds", "pendigits", "knowledge", "adult"]
#front_data = ["wine", "hepatitis", "wholesale", "mushroom"]

end_data = ["adult"]
front_data = []

for name in end_data+front_data:

    print name
    data = np.loadtxt(name+".csv", delimiter=",", dtype=str)

    for i in range(len(data[0])):
        
        attr_num = dict(Counter(data[:, i]))

        #numerical
        if len(attr_num) > 50:    

            if "?" in attr_num:
                sum_list = [float(key)*attr_num[key] for key in attr_num if key != "?"]
                mean = sum(sum_list)/(len(data)-attr_num["?"])

                for j in range(len(data)):
                    if data[j, i] == "?": data[j, i] = str(mean)

        #categorical
        else:
            
            attr_list = sorted([[key, str(j), attr_num[key]] for j, key in enumerate(attr_num)], key=lambda l:l[2], reverse=True)
            attr_map = {data[0]:data[1] for data in attr_list}

            if "?" in attr_map:
                if attr_list[0][0] != "?": attr_map["?"] = attr_list[0][1]
                else: attr_map["?"] = attr_list[1][1]

            for j in range(len(data)):
                data[j, i] = attr_map[data[j, i]]

    if name in end_data:
        data = np.concatenate((data[:, -1:], data[:, :-1]), axis=1)

    np.savetxt(name+".csv", data, delimiter=",", fmt="%s")
            
        
