Read me file for CSTS code

The repository includes:
    - code: CSTS main codes and code to construct artificial costs
    - dataset/RAC_dataset: dataset with costs
    - log: log files for each dataset

Artificial Costs Creation:
    Run in code/: 
        python2 find_price.py data_name/all SVC/random/uniform/clustering [reverse]
    Output:
        dataset/[data_name]_[method]_[r]_price.csv
    
    Utility:
        Generate the artificial costs for the dataset with specific way
        - uniform: uniform costs
        - random: random costs
        - SVC: reverse distance to the decision boundary
        - clustering: the method proposed in the paper(reverse distance cost)
        If 'reverse' is set, the clustering method will generate the distance cost

CSTS:
    Run in code/:
        python2 CSTS.py --data [dataset/all] --price_type [uniform/random/clustering/clustering_r/SVC] 
                        --base_learner [SVC/LogR] --alpha [int] --beta [float] --gamma [float]
    Output:
        print the AUC and write the following results in each stage(query every 1% of total cost) in the log file:
        - accuracy 
        - variance of accuracy
        - train accuracy by tree: the accuracy of training data if we use the tree to label all the instance
        - num of leaves 
        - best train accuracy by tree: the best accuracy on training data current tree could reach
        - num of query instances

    Detail:
        read_data.py: will read the corresponding dataset and its costs
        ML_model.py: the base learner, use CV to tune the parameters listed in the first few lines
        decision_stump: the revised decision stump algorithm to use the new metric to find the best split for a node

