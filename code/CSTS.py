import numpy as np
from math import log
from collections import Counter
import random, sys, copy, struct, os, time, datetime, gc, operator, argparse

from multiprocessing import Pool

from read_data import read_dataset
from decision_stump import ds_model, gini
from ML_model import Base_Tune_Model

class tree:
	def __init__( self, ID ):

		self._ID = np.asarray( ID )
		self._ID_set = set(self._ID)
		self._queried_ID = []
		self._L = -1
		self._n = 0
		self._label_prob = {}
		self._metric_score = 0
			
		self._type = "leaf"
		self._children = []
		self._dirty = 0
		self._aver_price = 0.01
		self._ds_model = ds_model(len(ID))
			
	def set_dirty(self, n):
		self._dirty = n
		for child in self._children: child.set_dirty(n)

	def select(self, Queried_Y):
		cand = self._ID_set.difference(self._queried_ID)
		return np.random.choice(list(cand))
											
	def update_bound(self, Queried_Y, typ, update_ID, parent_label_prob):
					
		if typ == "add" and update_ID in self._ID_set: self._queried_ID.append(update_ID)

                if len(self._queried_ID) == 0:
                    self._queried_ID = [i for i in self._ID if Queried_Y[i] != -1]
		self._n = len(self._queried_ID)
		train_y = Queried_Y[self._queried_ID]
		label_count = dict(Counter(train_y))
		self._L = max(label_count, key=label_count.get)
		
		prices = Price[self._queried_ID]
		self._aver_price = np.mean(prices)

		for i in set(Queried_Y):
			if i == -1: continue
                        if i not in self._label_prob: self._label_prob[i] = 0.
			if i not in label_count: label_count[i] = 0.
		
		#Just to update info when a new instance is quired
		if typ != "add": return
		
		worse_case = 1 - 1. / len(set(train_y))
		scale_param = ((np.max(prices) - np.min(prices))/2.)**2
		if scale_param == 0: scale_param = 1
		price_var = sum([x**2 for x in prices]) / float(len(prices)) - (sum(prices) / float(len(prices))) ** 2
		l_set = set(train_y)
		
		#tmp_label_dic = { int(i):float(self._label_count[i])/len(train_y) for i in set(All_y) }
		#root
		if len(self._ID) == len(Train_X): 
			for j in self._label_prob: parent_label_prob[j] = 1./len(self._label_prob.keys())
		
		#calculate the estimated label probability
		delta = reduce(operator.mul, [label_count[j] * (self._n - label_count[j]) for j in l_set])
		delta = np.power(delta, 0.5 / len(l_set))
		delta = (1. / self._n - 1. / len(self._ID)) * (1 + delta / (len(self._ID)**2))
		if delta < 1. / ALPHA:
			for j in self._label_prob:
				self._label_prob[j] = ALPHA * delta * parent_label_prob[j] + \
					                    (1 - ALPHA * delta) * label_count[j] / self._n
		else:
			self._metric_score = np.inf
			return 
		
		self._metric_score = (1 - BETA) * gini(self._label_prob) + \
							BETA * worse_case * price_var / scale_param
		
		#if parent: update children and find whether need re-construct
		#if child: find out whether ready to be splited
                if update_ID not in self._ID_set: return 
		if self._type == "parent":
			for child in self._children: 
				child.update_bound(Queried_Y, "add", update_ID, self._label_prob)
			if self._metric_score * len(self._ID) < sum([child._metric_score * len(child._ID) for child in self._children]):
				self.set_dirty(2)
		else:
			booll, label_dics, child_ID = self._ds_model.split(Train_X[self._ID], Queried_Y[self._ID], \
						    	Price[self._ID], self._label_prob, ALPHA, BETA)
			if booll: self.set_dirty(1)
	
	def find_compose( self ):

		self._score_compose = []
		if self._type == "parent":
			for child in self._children: self._score_compose.extend(child.find_compose())
		else:
			self._score_compose = [self]
		return self._score_compose

	def find_score(self, Queried_Y):
	
		self.update_bound(Queried_Y, "extend", None, None)
		
		# no label or only one kind of label
		if self._n == 0 or len(set(Queried_Y[self._queried_ID])) == 1: return
		# non-dirty parent, only update children
		if self._type == "parent" and self._dirty == 0:
			for child in self._children: child.find_score(Queried_Y)
			return
		
		self._dirty = 0
		self._children = []
		booll, label_dics, child_ID = self._ds_model.split(Train_X[self._ID], Queried_Y[self._ID], \
								    Price[self._ID], self._label_prob, ALPHA, BETA)

		# not ready to be split: it's a leaf
		if not booll: 
			self._type = "leaf"
			return
		
		# else are parents
		self._type = "parent"
		#print child_ID, label_dics
		for i, child in enumerate(child_ID):
			child_tree = tree(self._ID[child])
			child_tree._label_prob = label_dics[i]
			child_tree.find_score(Queried_Y)
			self._children.append(child_tree)

# Select a node in Puring to query 
def select(P):	

	if len(P) == 1: return 0
	prob = []
	for i,node in enumerate(P): 
	        prob.append((len(node._ID) - node._n) * (gini( node._label_prob ) + 0.001) / node._aver_price)
	prob = np.asarray(prob) / sum(prob)
	return np.random.choice(range(len(P)), p=prob)

def cal_prop(labels, label_dic):
	prop = np.asarray([float(label_dic[i]) if i in label_dic else 0. for i in labels])
	return prop/sum(prop)

def KL_divergence(P, Q):
	P = np.asarray(P, dtype=np.float)
	Q = np.asarray(Q, dtype=np.float)
	return np.sum([p*np.log(p/q) if p != 0 and q != 0 else 0 for p, q in zip(P,Q)])
								
def main_algorithm(data_tree) :
				
	np.random.seed(data_tree)
        ID = range(len(Train_X))		
	section_qnum, results_qnum = [(Q/30.)*i for i in range(1,31)], []
	section_ptr = 0
	results_dic, out_put_dic = {}, []

	first_node = tree(ID)
	P = [first_node]
	Q_C_num = 0
	q_num, Question = 0, {}
	Queried_Y = -np.ones(len(Train_Y))

	y_set = set(Train_Y)

	#start algorithm
	while(q_num < Q):
					
		# ask label & modified tree
		dirty_num = 0
		while dirty_num < GAMMA * len(P) and q_num < section_qnum[section_ptr]:
			z = P[select(P)].select(Queried_Y)
			Question[z], Queried_Y[z] = 0, Train_Y[z] 
			first_node.update_bound(Queried_Y, "add", z, {}) 
			dirty_num = sum([1 if leaf._dirty!=0 else 0 for leaf in P])
			q_num += Price[z]
			Q_C_num += 1
			
		# update A, compute new score for each node and update P & L
		first_node.find_score(Queried_Y)
		P = first_node.find_compose()

		best_acc = 0.
		cand_y, b_cand_y, fill_gini =np.zeros(len(Train_X)), np.zeros(len(Train_X)), np.zeros(len(Train_X))
		for i, node in enumerate(P):
			cand_y[node._ID] = node._L
			fill_gini[node._ID] = gini(node._label_prob)
			label_in_nodes = dict(Counter(Train_Y[node._ID]))
			best_L = max(label_in_nodes, key=label_in_nodes.get)
			b_cand_y[node._ID] = best_L
		best_acc /= float(len(Train_X)-len(Question))
		
		non_quired_index = np.delete(range(Train_X.shape[0]), Question.keys()) 
		fill_gini = fill_gini[non_quired_index]
		fill_x = Train_X[non_quired_index]
		fill_y = cand_y[non_quired_index]
		b_cand_y = b_cand_y[non_quired_index]
		acc = list(Train_Y[non_quired_index]-fill_y).count(0)/float(len(Train_X)-len(Question))
		best_acc = list(Train_Y[non_quired_index] - b_cand_y).count(0)/float(len(Train_X)-len(Question))
		if len(set(Queried_Y[Question.keys()])) != 1:
			score = Base_Tune_Model(ARGS.base_learner, Train_X[Question.keys()], Train_Y[Question.keys()], fill_x, fill_y, fill_gini, Test_X, Test_Y)
		else:
			score = float(sum([ 1 for i in Test_Y if i == Train_Y[0] ]))/len(Test_Y)
			
		#print "Queried instance num %d, Queried costs %.4f, base model acc %.4f, best tree train acc %.4f, tree train acc %.4f"\
		#        %(Q_C_num, q_num, score, best_acc, acc)
		
		while section_ptr < 30 and section_qnum[section_ptr] < q_num: section_ptr += 1
		results_dic[q_num] = np.asarray([score, acc, len(P), best_acc, Q_C_num])
		results_qnum.append(q_num)

	first, P = None, None
	
	section_ptr, results_ptr = 0, 0
	while section_ptr < 30:
		while results_qnum[results_ptr] <= section_qnum[section_ptr]: results_ptr += 1
		while section_ptr < 30 and section_qnum[section_ptr] <= results_qnum[results_ptr]:
			out_put_dic.append(((section_qnum[section_ptr] - results_qnum[results_ptr-1]) * results_dic[results_qnum[results_ptr]] + \
								(results_qnum[results_ptr] - section_qnum[section_ptr]) * results_dic[results_qnum[results_ptr-1]]) / \
								(results_qnum[results_ptr] - results_qnum[results_ptr-1]))
			section_ptr += 1

	return out_put_dic
								
if __name__ == '__main__':
		
	Start_time = time.time()

        PARSER = argparse.ArgumentParser('')
        PARSER.add_argument('--data', type=str, default='all_data')
        PARSER.add_argument('--price_type', type=str, default='clustering')
        PARSER.add_argument('--base_learner', type=str, default='LogR')
        PARSER.add_argument('--alpha', type=int, default=2)
        PARSER.add_argument('--beta', type=float, default=0.5)
        PARSER.add_argument('--gamma', type=float, default=0.3)

        ARGS = PARSER.parse_args()
        ALPHA, BETA, GAMMA = ARGS.alpha, ARGS.beta, ARGS.gamma

	if ARGS.data != "all_data":
		names = [ARGS.data]
	else:
		names = ["Australian", "breast", "diabetes", "german", "glass", "heart", "liver", "sonar", "vehicle", "vote", "wine", \
		            "hepatitis", "wholesale", "mushroom", "abalone", "yeast", "ttt", "nursey", "seeds", "pendigits", "knowledge", "adult"]
		index = [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 15, 18, 20, 13, 17, 21]	  
		names = np.asarray(names)[index]		
			
	for name in names:
		print name
		out_fp = open("../"+"log/"+name+".txt", "a+")
                out_fp.write(time.ctime()+"\n")
		out_fp.write("_".join(sys.argv[1:])+"\n")
		
		print "start read data @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		heuristic_costs, Price, Train_X, Train_Y, Test_X, Test_Y = read_dataset( name, "", read_Price=ARGS.price_type)
		Q = sum(Price) * 0.3
		print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))

		print "start algorithm @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		p = Pool(10)	
		results = np.asarray(p.map(main_algorithm, [i for i in range(10)]))
		print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		
		y = [[np.mean(results[:, i, 0]) for i in range(30)], \
			 [np.var(results[:, i, 0]) for i in range(30) ]] +\
			[[ np.mean(results[:, i, k]) for i in range(30) ] for k in range(1, 5)]
                print "AUC:", sum(y[0]) - (y[0][0] + y[0][29])/2

                line_name = ["ACC,", "ACC_VAR,", "tree_train_acc,", "leaf_num,", "best_tree_train_acc,", "query_num,"]
		for i in range(len(y)): 
		    out_fp.write(line_name[i] + ",".join([ str(result) for result in y[i] ])+"\n")
		print "Done @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		p.close()
		p.join()
		out_fp.close()
		
