import numpy as np
from math import log
import copy, operator
from collections import Counter

def entropy( probs ):
	en = 0.
	for i in probs:
		if i<0.001: continue
		en += i * np.log(i)
	return -1*en
	
def gini( probs ):
	return 1 - sum([probs[p]**2 for p in probs])

def list_gini( probs ):
	return 1 - sum([p**2 for p in probs])

def decide_split( X, Y, p, p_square, label_index, org_score, label_prob, t, gamma ):

        best_split_num = 0
	total_label_num, length = len(label_index), len(Y)
	best_score, best_thres, best_cent = org_score*length + 0.00001, np.inf, 0
	best_child_label_prob = [ {}, {} ]
	left_label_num = 0
	
	data = np.column_stack((X,Y))
	data = data[np.argsort(data[:, 0])]
	price = np.column_stack((X[label_index], p, p_square))
	price = price[np.argsort(price[:, 0])]

	org_label_count = dict(Counter(Y[label_index]))
	label_count = np.zeros((2, int(max(label_prob.keys())+1)))
	for i in org_label_count: label_count[1, int(i)] = org_label_count[i]
	child_p = np.zeros((2, 2))
	child_p[1] = np.asarray([sum(p), sum(p_square)])
	
	for i in range(0, length-1):
		if data[i,1] != -1:
			label_count[0, int(data[i,1])] += 1
			label_count[1, int(data[i,1])] -= 1
			child_p[0] += np.asarray([price[left_label_num, 1], price[left_label_num, 2]])
			child_p[1] -= np.asarray([price[left_label_num, 1], price[left_label_num, 2]])
			left_label_num += 1
		
		#too tiny split / no label on the left / same x value as the next
		if i < 1 or left_label_num == 0 or data[i,0] == data[i+1,0]: continue
		# no label on the right
		if left_label_num == total_label_num: break
			
		score = 0
		sizes = [i+1, length-i-1]
		child_label_prob = [ {}, {} ]
		for k in range(2):
			
			if k == 0: n = float(left_label_num)
			else: n = float(total_label_num - left_label_num)
			
			child_label_prob[k] = copy.copy(label_prob)
			if n > t * sizes[k]/(sizes[k] + t):
				delta = reduce(operator.mul, [label_count[k,int(j)] * (n - label_count[k, int(j)]) for j in label_prob])
				delta = np.power(delta, 0.5/len(label_prob))
				delta = (1. / n - 1. / sizes[k]) * (1 + delta / (n**2))
				if delta < 1. / t:
					for j in label_prob: 
						child_label_prob[k][int(j)] = t * delta * label_prob[int(j)] + \
										    (1 - t * delta) * label_count[k, int(j)] / n
					score += ((1 - gamma) * gini(child_label_prob[k]) + \
								gamma * (n * child_p[k, 1] - child_p[k, 0]**2) / (n**2)) * sizes[k]
				else:
					score += np.inf
			else:
				score += np.inf

		score = round(score, 6)
		if score < best_score or (score == best_score and score < round(org_score*length, 6) and sizes[0] * sizes[1] > best_cent):
			best_score, best_thres = score, (data[i+1,0] + data[i,0]) / 2.
			best_child_label_prob = copy.copy(child_label_prob)
			best_cent = sizes[0] * sizes[1]
			best_split_num = i+1
			
	return best_score / float(length), best_thres, best_child_label_prob, best_split_num

class ds_model:
	
	def __init__( self, size ):

		self._entropy_list = []
		self._size = size
		
		self._feature_index = -1
		self._split_thres = 0

	def predict_child( self, X ):
			if X[self._feature_index] >= self._split_thres: return 1
			else: return 0
	
	def split(self, X, Y, price, label_prob, t, gamma):			

                b_split_num = 0

		label_index = [i for i, l in enumerate(Y) if l != -1]
		max_value, min_value = np.max(price[label_index]), np.min(price[label_index])
		std_value = (max_value-min_value)/2.
		if std_value != 0: p = (1 - 1. / len(set(Y))) * price[label_index] / std_value
		else: p = price[label_index]
		p_square = np.asarray([i**2 for i in p])
		org_score = (1 - gamma) * gini(label_prob) + gamma * (len(label_index) * sum(p_square) - sum(p)**2) / (len(label_index)**2)
		price_sum = np.sum(p)
		
		best_score, best_thres, index = org_score, 0, -1
		best_child_label_probs = None
				
		for i in range(len(X[0])):
		
			score, thres, child_label_probs, split_num =  decide_split( X[:,i], Y, p, p_square, label_index, org_score, label_prob, t, gamma)
			if score < best_score:
				best_score, best_thres, index = score, thres, i
				best_child_label_probs = copy.copy(child_label_probs)
			        b_split_num = split_num

		child = [ [], [] ]
		if index == -1:	return False, None, None
		for i, data in enumerate(X):
			if data[index] >= best_thres: child[1].append(i)
			else: child[0].append(i)
		#print index, b_split_num, len(child[0]), len(child[1]), label_prob, best_child_label_probs
		self._feature_index, self._split_thres = index, best_thres
		return True, best_child_label_probs, child

