#importing the MNIST data set
import numpy as np
import os, struct, random
from array import array

from sklearn.preprocessing import StandardScaler
from sklearn.datasets import fetch_20newsgroups_vectorized

def read( dataset, path ):
   
	if dataset is "training":
		fname_img = os.path.join(path, 'train-images.idx3-ubyte')
		fname_lbl = os.path.join(path, 'train-labels.idx1-ubyte')
	elif dataset is "testing":
		fname_img = os.path.join(path, 't10k-images.idx3-ubyte')
		fname_lbl = os.path.join(path, 't10k-labels.idx1-ubyte')
	else:
		raise ValueError, "dataset must be 'testing' or 'training'"

	flbl = open(fname_lbl, 'rb')
	
	magic_nr, size = struct.unpack(">II", flbl.read(8))
	lbl = array("b", flbl.read())
	flbl.close()

	fimg = open(fname_img, 'rb')
	magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
	img = array("B", fimg.read())
	fimg.close()

	ind = [ k for k in xrange(size) ]
	images =  []
	labels = []
	for i in xrange(len(ind)):
		images.append( np.asarray( img[ ind[i]*rows*cols:(ind[i]+1)*rows*cols ] ) )
		labels.append( lbl[ind[i]] )

	return np.asarray(images), labels

#importing the other data set
def readfile(name, read_Price):
	Price = []
	data = np.loadtxt("../dataset/"+name+".csv", delimiter=",", dtype=np.float32)
	if read_Price != "": 
		Price = np.loadtxt("../dataset/"+name+"_"+read_Price+"_price.csv", delimiter=",", dtype=np.float32)
			#if np.std(Price) != 0: Price /= np.std(Price)
	return name, data[:,0], data[:,1:], Price
	
def read_dataset( dataset, way, read_Price="" ):
	
	heurestic_cost = []
        if dataset == "MINST":
		
		dis_file = "MINST_dis.csv"
		Train_X, Train_Y = read( "training", "../MINST" )
		Test_X, Test_Y = read( "testing", "../MINST" )
		train_size, test_size = 46000, len(Test_Y)
		
		Train_X = Train_X[ :train_size, : ]
		Test_X, Test_Y = Test_X[ :test_size, : ], Test_Y[ :test_size ]
		
	elif dataset == "20news":
		
		dis_file = "20news_dis.csv"
		bunch = fetch_20newsgroups_vectorized(subset="train")
		Train_X, Train_Y  = np.asarray(bunch.data.todense()), bunch.target
		bunch_test = fetch_20newsgroups_vectorized(subset="test")
		Test_X, Test_Y = np.asarray(bunch_test.data.todense()), bunch_test.target
		del bunch, bunch_test
		
		train_size = len(Train_X)
	
	elif dataset == "RAC":

		Price = np.loadtxt("../RAC_dataset/clean_all.time", dtype=float)
		heurestic_cost = np.loadtxt("../RAC_dataset/hurest.time", dtype=float)
		data = np.loadtxt("../RAC_dataset/clean_all.dat", dtype=int, delimiter=",")
		Train_X = data[:, 1:]
		Train_Y = data[:, 0]

		train_size = len(Train_X)*4/5
		#Train_X = StandardScaler().fit_transform(Train_X)
		
		random.seed(1118)
		ran = range(len(Train_X))
		random.shuffle(ran)
		Train_X, Train_Y = Train_X[ ran ], Train_Y[ ran ]
			
		Test_X, Test_Y = Train_X[ train_size: ], Train_Y[ train_size: ]
		Train_X = Train_X[ :train_size ]
		Price = Price[ ran[:train_size] ]
		heurestic_cost = heurestic_cost[ ran[:train_size] ]
		
	else:
		name, Train_Y, Train_X, Price = readfile(dataset, read_Price)
		Train_X = StandardScaler().fit_transform(Train_X)
		train_size	= len(Train_X)*4/5
		
		random.seed(917)
		ran = range(len(Train_X))
		random.shuffle(ran)
		Train_X, Train_Y = Train_X[ran], Train_Y[ran]
			
		Test_X, Test_Y = Train_X[train_size:], Train_Y[train_size:]
		Train_X = Train_X[:train_size]
		Price = Price[ran[:train_size]]
	
	return heurestic_cost, Price, Train_X, Train_Y, Test_X, Test_Y
