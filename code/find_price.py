import numpy as np
import random, sys, time, datetime

from sklearn.svm import SVC
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from read_data import readfile

data_set = sys.argv[1]
#SVC, random, Clustering
way = sys.argv[2]

if __name__ == '__main__':
		
	Start_time = time.time()
		
        if data_set == "all":
		names = ["Australian", "breast", "diabetes", "german", "glass", "heart", "liver", "sonar", "vehicle", "vote", \
		         "wine", "hepatitis", "wholesale", "mushroom", "abalone", "yeast", "ttt", "nursey", "seeds", "pendigits", "knowledge"]
	else:
                names = [data_set]
	
	for name in names:
			
		print name
                if len(sys.argv) > 3 and sys.argv[3] == "reverse": out_fp = open("../dataset/"+name+"_"+way+"_r_price.csv", "w")
                else: out_fp = open("../dataset/"+name+"_"+way+"_price.csv", "w")
		
		#print "start read data @", str(datetime.timedelta( seconds=time.time()-Start_time ))
                name, Train_Y, Train_X, p = readfile( name, "" )
		Train_X = StandardScaler().fit_transform(Train_X)
		#print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		
                if way == "random":
                    final_weight = map(abs, np.random.gamma(1, 2, len(Train_X)))
                elif way == "uniform":
                    final_weight = np.ones((len(Train_X), 1))
                else:

		    print "start algorithm @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		    model = SVC(C=100, kernel="rbf", decision_function_shape='ovr')
    	    	    model.fit(Train_X, Train_Y)
    	    	    print model.score(Train_X, Train_Y), len(set(Train_Y)), Train_X.shape
		    print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		
		    print "find weight @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		    all_distances = model.decision_function(Train_X)
		    distance = [np.min(abs(distances)) for distances in all_distances]
		    
                    if way == "SVC":
                        if len(sys.argv) > 3 and sys.argv[3] == "reverse": weight_base = np.asarray(distance)
                        #else: weight_base = np.asarray([(1./dis) for dis in distance])
                        else: weight_base = np.asarray([np.max(distance)-dis+np.min(distance) for dis in distance])
		        stdiv = np.std(weight_base)
		        final_weight = np.asarray([[max(0.001, mean) for mean in weight_base]]).T
                    elif way == "clustering":
		        model = KMeans(n_clusters=max(50, len(Train_X)/20), n_jobs=-1)
		        cluster_index = model.fit_predict(Train_X)

                        mean_dis = np.zeros((max(50, len(Train_X)/20), 2))
                        for index, dis in zip(cluster_index, distance): mean_dis[index] += np.asarray([1., dis])
                        final_weight = np.zeros(len(Train_X))
                        if len(sys.argv) > 3 and sys.argv[3] == "reverse":
                            for i, index in enumerate(cluster_index): final_weight[i] = max(mean_dis[index, 1]/mean_dis[index, 0], 0.001)
                        else:
                            for i, index in enumerate(cluster_index): final_weight[i] = max(mean_dis[index, 0]/mean_dis[index, 1], 0.001)

		    print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		
		print "find weight @", str(datetime.timedelta( seconds=time.time()-Start_time ))
                if way != "uniform" and np.min(final_weight)*5 > np.max(final_weight): 
                    final_weight -= np.min(final_weight)-((np.max(final_weight)-np.min(final_weight))/4.)
                if way == "uniform": np.savetxt(out_fp, final_weight, fmt="%f")
                else: np.savetxt(out_fp, final_weight/np.std(final_weight), fmt="%f")
		print " finish @", str(datetime.timedelta( seconds=time.time()-Start_time ))
		
