import numpy as np
import itertools
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.cross_validation import cross_val_score, KFold
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsOneClassifier
from collections import Counter

model_params = {"LogR":[{"C":0.01},{"C":0.1},{"C":1},{"C":10},{"C":100}],\
				"LogR_ovo":[{"C":0.01},{"C":0.1},{"C":1},{"C":10},{"C":100}],\
				"DT":[{"max_depth":5},{"max_depth":10},{"max_depth":15},{"max_depth":20},{"max_depth":25}],\
				"RF":[{"max_depth":3, "n_estimators":600},{"max_depth":10, "n_estimators":600},{"max_depth":5, "n_estimators":600},{"max_depth":15, "n_estimators":600}],\
				"SVM_RBF":[{"C":0.01},{"C":0.1},{"C":1},{"C":10},{"C":100}],\
				"SVM_P_all":[{"C":p[0], "kernel":p[1], "degree":p[2], "coef0":p[3]} for p in itertools.product([0.01, 0.1, 1, 10, 100], ["poly"], [2], [-10, -1, 0, 1, 10])],\
				"SVM_P3_c1":[{"C":0.01, "kernel":"poly", "coef0":10},{"C":1, "kernel":"poly", "coef0":10},{"C":0.1, "kernel":"poly", "coef0":10},{"C":10, "kernel":"poly", "coef0":10},{"C":100, "kernel":"poly", "coef0":10}],\
				"SVM_P":[{"C":0.01, "kernel":"poly", "degree":2},{"C":1, "kernel":"poly", "degree":2},{"C":0.1, "kernel":"poly", "degree":2},{"C":10, "kernel":"poly", "degree":2},{"C":100, "kernel":"poly", "degree":2}],\
				"SVM_P_ovr":[{"C":0.01, "kernel":"poly", "degree":2, "decision_function_shape":'ovr'},{"C":1, "kernel":"poly", "degree":2, "decision_function_shape":'ovr'},{"C":0.1, "kernel":"poly", "degree":2, "decision_function_shape":'ovr'},{"C":10, "kernel":"poly", "degree":2, "decision_function_shape":'ovr'},{"C":100, "kernel":"poly", "degree":2, "decision_function_shape":'ovr'}],\
				}
gini_thres = [0, 0.1, 0.2, 0.3, 0.4] 
#gini_thres = [0, 0.1, 0.2] 
#gini_thres = [-1] 
model_dict = {"LogR":LogisticRegression, "LogR_ovo":LogisticRegression, "RF":RandomForestClassifier, "DT":DecisionTreeRegressor, "SVM_RBF":SVC, "SVM_P3":SVC, "SVM_P":SVC, "SVM_P_all":SVC}

def Base_Model(model_type, train_x, train_y, all_train_x, all_train_y, test_x, test_y, typ):
   
	train_x = np.asarray(train_x)
	best_cv_score, index = -1, -1
	kf = KFold(len(train_x), n_folds=min(len(train_x),4), shuffle=True)
	for i, param in enumerate(model_params[model_type]):
		model = model_dict[model_type](**param)
		score = []
		try:
			score = cross_val_score(model, train_x, y=train_y, scoring="accuracy", cv=min(4, len(train_x)))
		except:
			continue
		if np.mean(score[0]) > best_cv_score:
			best_cv_score, index = np.mean(score), i
	
	if index == -1: index = 1
	param = model_params[model_type][index]
	if typ == "prob" and "SVM" in model_type:
		param["probability"] = True
	model = model_dict[model_type](**param)
	model.fit(train_x, train_y)

	if typ == "prob":
		return model.predict_proba(train_x), model.score(all_train_x, all_train_y), model.score(test_x, test_y)
	else:
		return model.score(all_train_x, all_train_y), model.score(test_x, test_y)

def Base_Tune_Model(model_type, train_x, train_y, fill_train_x, fill_train_y, fill_train_gini, test_x, test_y):
	
	max_thres, min_thres = np.max(fill_train_gini), np.min(fill_train_gini)
	best_cv_score, index, best_gini_thres = -1, -1, -1
	kf = KFold(len(train_x), n_folds=min(len(train_x),4), shuffle=True)

	for i, param in enumerate(model_params[model_type]):
		if model_type != "LogR_ovo": model = model_dict[model_type](**param)
		else: model = OneVsOneClassifier(LogisticRegression(**param))
		#for thres in prob_thres:
		for thres in gini_thres:
			if thres < min_thres or thres > max_thres: continue
			add_index = [k for k, j in enumerate(fill_train_gini) if j < thres]
			cv_train_x, cv_train_y = fill_train_x[add_index], fill_train_y[add_index]
			score = []
			try:
			        for train, test in kf:
					kf_train_x = list(train_x[train]) + list(cv_train_x)
					kf_train_y = list(train_y[train]) + list(cv_train_y)
					score.append(model.fit(kf_train_x, kf_train_y).score(train_x[test], train_y[test]))
					#kf_train_x = list(train_x) + list(cv_train_x)
					#kf_train_y = list(train_y) + list(cv_train_y)
					#score.append(model.fit(kf_train_x, kf_train_y).score(test_x, test_y))
	                except: continue
			if np.mean(score) > best_cv_score:
				best_cv_score, index, best_gini_thres = np.mean(score), i, thres
	#if index == -1: index = 4
	if index == -1: index = 1
	if model_type != "LogR_ovo": model = model_dict[model_type](**model_params[model_type][index])
	else: model = OneVsOneClassifier(LogisticRegression(**model_params[model_type][index]))

	add_index = [i for i, j in enumerate(fill_train_gini) if j <= best_gini_thres]
	addi_train_x, addi_train_y = fill_train_x[add_index], fill_train_y[add_index]
	model.fit(list(train_x)+list(addi_train_x), list(train_y)+list(addi_train_y))
	return model.score(test_x, test_y)
	
def DT_model(train_x, train_y, test_x):
	
	best_cv_score, index = -1, -1
	for i, param in enumerate(model_params["DT"]):
		model = DecisionTreeRegressor(**param)
		try:
			score = cross_val_score(model, train_x, y=train_y, scoring='neg_mean_squared_error', cv=4)
		except:
			continue
		if np.mean(score) < best_cv_score:
			best_cv_score, index = np.mean(score), i

	model = DecisionTreeRegressor(**model_params["DT"][index])
	model.fit(train_x, train_y)

	return model.predict(test_x)

